window.AppConfig = {
  subDomain: configInit.subDomain,
  baseApiUrl: 'https://localhost:8000/',
  apiUrl: 'https://localhost:8000/api/v1/',
  webSocketUrl: 'wss://localhost:8000/ws',
  clientId: '1',
  clientSecret: 'Y8lYUupAIhNCDjKfDoYNmqlffnHsmlNFMwarJCp2',
  IntervalRefreshData: 120000,
  dateFormat: 'DD-MM-YYYY hh:mm:ss A',
  shortDateFormat: 'DD-MM-YYYY'
}
